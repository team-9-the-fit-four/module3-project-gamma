### Welcome
* Endpoint path: /home
* Endpoint method: GET

* Response: Home
* Response shape:
  ```json
  {
    "workout": [
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      }
    ]
  }
  ```

### Log in
* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
  ```json
  {
    "account": {
      "token": string
    }
  }
  ```

### Log out
* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
  ```json
  true
  ```

### Sign Up
* Endpoint path: /signup
* Endpoint method: POST

* Response: Welcome
* Response shape:
  ```json
  {
    "signup": [
      {
        "email": string,
        "password": string,
        "first_name": string,
        "last_name": string,
        "age": number,
        "height": number,
        "weight": number,
      }
    ]
  }
  ```

### Today's Workout
* Endpoint path: /workout
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: List of Exercises
* Response shape:
  ```json
  {
    "exercises": [
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      },
      {
        "name": string,
        "type": string,
        "muscle": string,
        "equipment": string,
        "difficulty": string,
        "instructions": string,
      }
    ]
  }
  ```

### History
* Endpoint path: /history
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: History of workouts
* Response shape:
  ```json
  {
    "Day": [
      {
        "date": string,
        "workouts": {
          "name": string
        },
        "completed": string
      }
    ]
  }
  ```

### Profile
* Endpoint path: /profile
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Account information
* Response shape:
  ```json
  {
    "profile": [
      {
        "first_name": string,
        "last_name": string,
        "age": number,
        "height": number,
        "weight": number,
      }
    ]
  }
  ```

### Journaling
March 10, 2023
Finished design to a satisfactory point and finished clean up to meet flake8 standards. Went through the code deleting useless parts. Finished Readme.

March 9, 2023
Finished test for deleting a workout. Continued work on design with css. Back and forth about what theme we want.

March 8, 2023
Finished the test for updating workout. Required quit alot of thought as it was not as straight foward as it seems. Did a lot of css editing.

March 7, 2023
Finished work on updating profiles. We had started on it but the save was not working. Required changing backend update but finally got it working.

March 6, 2023
Fixed issues with redirects on login, signup, creating exercise.

March 3, 2023
Reorganized the workout list as well as adding functionality to adding an exercise. Added a new slot to the workout table for pictures.

March 2, 2023
Fixed update so that the name of a workout can be changed.

March 1, 2023
Fixed create workout form.
February 28, 2023
Changed login logout to use redux. Changed Signup to redux.

February 27, 2023
Finished login, signup, logout on frontend. Using redux.

Februuary 24, 2023
Finished work on backend. Fixed problem with creating exercise. Started planning for frontend.

February 23, 2023
Finished functions for creating an exercise from the api. Also has the ability to add the exercises to the workouts.

February 22, 2023
Finished the random daily exercises. Finished the workout endpoints and the user endpoints.

February 21, 2023
Almost finished daily exercises. At the stage of getting random exercises but working at getting that stored.

February 16, 2023
Finished authentication. We now have endpoints for creating a user, signing in and signing out. Started on creating tables for the api and figuring out a way for our database to get information from the api.

February 15, 2023
Got the meat of authentication done. Hit a block trying to create Accounts. Will work on this tomorrow.

February 14, 2023
Finished making our data base. Integrated pg admin. Began work on authentication.

February 13, 2023
Created our database named hustle. Though we used the learn multiple databases, we will only use one database.

February 10, 2023
Today we finalized our API endpoints and created our tables. We also finalized our wireframes by resizing the scope of the project and creating stretch goals.

February 9, 2023
Just starting with the project. Finishing the API endpoints as well as beginning journaling for the first time. Starting work on the issues as well.

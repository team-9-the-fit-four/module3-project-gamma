### Journaling

March 10, 2023
Changed background to animated clouds with raindrops. Learned how to use Three.js library for animated 3D canvas backgrounds and z-indexes in CSS. Reviewed every file to clean up code and remove unused imports and variables. Ran black and flake8 to resolve linter issues. Project is ready for deployment.

March 9, 2023
Added BMI calculator with radio slider icons to user profile page. Fixed all errors and warnings in the dev console. Refactored code and formatting. Updated buttons with animations and further styling. Updated favicon icon and name on all website tabs with the Household Hustle logo and name.

March 8, 2023
Wrote more unit tests for workout routes, more CSS styling for main page, form pages, and list pages. Working on more styling for dark neon effects.

March 7, 2023
Added background theme music to website with play and pause buttons and volume slider. Created trainers SQL table with queries and routes to create and view trainers. Working on fixing React DOM warnings and CSS styling.

March 6, 2023
Added redirects for login, signup, and create exercise page. More CSS styling.

March 3, 2023
CSS styling concepts. Implemented function to add exercise from the workout details page.

March 2, 2023
Added feature for user to update name of a workout on the same page.

March 1, 2023
Fixed create workout form.

February 28, 2023
Added authorization to users, exercises, and workouts routes. Implemented Redux to login, logout, and signup.

February 27, 2023
Worked on frontend login, logout, signup, workout lists, and workout form

February 24, 2023
Backend complete, started Frontend

February 23, 2023
Created new exercise table, updated users and exercises queries and routers

February 22, 2023
Added workouts SQL table, workouts HTTP POST, GET, GET, PUT, DELETE functioning, working on users HTTP

February 21, 2023
External exercise API is working, created a few routes and queries, added all 16 muscle names to muscles SQL table

February 16, 2023
Finished authenticator, create user, login, logout, added 3 more table migrations

February 15, 2023
Created UserRepo class and router with function to create users.

February 14, 2023
Set up pgAdmin account, created users table, started to work on authentication.

February 13, 2023
Added directories and files to boilerplate, created Docker containers, images, and volumes, added PostgresQL database.

February 10, 2023
Explored explorations, finalized API endpoints, created tables, and reduced scope of project.

February 9, 2023
Cloned repository and getting project set up. Completed wireframing and API design.

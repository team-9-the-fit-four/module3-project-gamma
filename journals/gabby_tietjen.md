March 10th 2023,
Went through and ran flake8 and black.  Cleaned all code and got rid of anything we didn't use.  Refactored our code, and checked our test one more time,
along with functionality.

March 9th 2023,
Finished all functionally that we wanted to change.  Added a BMI calculator.  All test are done and passing.  I would say we are done at this point.
Added a lot of styling today.  All red errors are gone.  3 of 4 test are completed.

March 7th 2023,
functionality was finished up today.  Mainly working on clearing any console errors we are getting along with adding some fun features like music and trainers page.  Started test, still not done but working on it.

March 6th 2023,
Add and delete features added to the exercise list.  Profile page and route completed.  Responsive nav bar figured out.  Starting on test.

March 2nd 2023,
Started working on the CSS of website.  Create a workout page is up and running.  Next step is to get the list of workouts to populate on the form.

February 28th 2023,
Changed accounts and exercises to be locked.  Changed login and logout to redux.  Got list of exercises to show up on workout page.

February 27th 2023,
Finished front end login, logout, and signup. Started the workout list and workout form. Had to change a few little things in our backend to make sure the front end would work properly.

February 23rd 2023,
Made a new exercise table. Made our three routes for exercises. I believe we can start on front end first thing tomorrow morning.

February 22nd 2023,
Made good progress today. Created our CRUD route for workouts. Still working on why we are not getting our status response when there is an error. All our routes for today are completed as well.

February 21st 2023,
Our end points are working now to be able to hit the API and pull the workouts. We were able to make it so that it only pulls 5. Added all of the muscle names to SQL table. Continuing to work on the random number generator. We have a couple more routes and queries to make, then we should be able to move on to the front end.

February 15th 2023
Created UserRepo to create a user. Also created a route for that. The big blocker for today was trying to figure out swagger and how to use that to diagnose problems.

February 14th 2023,
Got through admin, it was up and running after some help from Riley. We created a users table, with it's respected columns. Started Authentication, just got the bones laid out.

February 13th 2023,
Started adding in the directories and files that were needed. Created our docker container and volumes. Also created the database today, hoping to be able to
start authentication tomorrow.

February 10th 2023,
Went through the explorations that were released. I feel like I'm going to have to watch these again once I dive into the project.
Very helpful though, and I think they will come in handy.

February 9th 2023,
Just starting our project.  
Everyone has created their journal files, and we are adding in our wireframing PNGs and API Design files.
Downloaded all necessary extensions as well.

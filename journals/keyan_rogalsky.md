### Journaling
March 10, 2023
Fixed up the formatting with flake8 advisories. Recreated the Docker
components and it all seems to be in running order. Kenny added more
styling to site, like an animated background and card transparency.
Changed up the linking of trainers.js file so that it didn't rely
on the localhost link.


March 9, 2023
Finally managed to finish the delete_workout test. More styling work
has been done on the site, changeing up the colors of the cards, text,
and buttons. Added BMI calculator with a slider to adjust and scale.
All our unit tests are functioning as intended. Changed the favicon
and tab title to align with the site: H "Household Hustle".


March 8, 2023
Fixed up Toaster notifications a bit. Started on unit tests. I'm
working on the deleteWorkout function. getWorkout and updateWorkout
have been completed. Kenny's been making the site look very nice.
Changed up the style of the home/main page, login/signup pages,
workouts, etc.
Going to work on: testDeleteWorkout, profile toast size,
workout picture size dimensions, double toast on login


March 7, 2023
Added Toaster notifications, wasn't able to add the scroll bar to the
workout exercises table. Looked into switching over to Pivot Tables, but
wouldn't be able to switch it all over without major, possibly code breaking,
issues as of now. Kenny added a neat music player, it'll continuously play
while navigating the site except for logging in.


March 6, 2023
Managed scroll bars for the exercises to reduce the size on the instructions.
Fixed up issues with redirects and keeping the user from being logged out
unintentionally.


March 3, 2023
Workouts can now have pictures added to help identify. Improved
functionality to adding exercises to workout lists and cleaned up
the workout lists.


March 2, 2023
Allowed the changing of Workout names.


March 1, 2023
Back online towards the middle of the day, fixed up the workout
forms.


February 28, 2023
Day 2 with no internet. Same as yesterday. Switched up the signup and
login to Redux.


February 27, 2023
Internet out. Was only able to provide meager support.
Group worked on the frontend with the login/logout, signup, workout lists & forms.


February 24, 2023
Got the exercise get and delete routers fixed. "Backend complete!"


February 23, 2023
Created a new table, routes for the exercise table, got all routes except for get and delete.


February 22, 2023
Made the ultimate decision to switch up what we planned to do for the
website. Going to emulate the other workout group's site in that we're
going to allow the user to create a workout with the exercises provided
from the API through us so that the user can organize them into
workouts for themselves. Added a workouts table and workouts routes
are set up. Current block is for users.


February 21, 2023
On the right track. Able to call to the Exercise API from API-Ninjas and get queries back.
Allowed our muscles table to hold the different muscle groups. Created more routes.
Had some minor errors we were able to overcome with file placement and code revision:
    - keys.py being within the API directory as opposed to the migrations (Me problem)
    - IndexError with inserting data into the muscles table
        etc.
Almost have pulling the workouts for the homepage/today's workout page. Currently it randomizers every time it hits the endpoint.
Also having an issue with implementing "History" for a user, isn't giving us the prompts we need it to in the Swagger.


February 16, 2023
Got our create user, login, logout, and authenticator squared away with successful testing on Swagger.
Managed to finish up adding 3 tables after struggling with a SyntaxError and other misc. bugs.
Plan to figure out a way to accomplish posting a random workout a day like we intended with the website.

February 15, 2023
We created UserRepo so that we can create users, worked on doing the authentication for them but were having issues actually testing it on the FastAPI/docs.
The last issue we ended with for the night was this error when we would try to actually create a user: "TypeError: 'int' object is not subscriptable."

February 14, 2023
After much trial & error, finished the "Project Advice" module in Learn for our code.
That entails setting up our Fast-API PGAdmin. Riley mentioned that some groups don't end up actually using it, but we hope to use it in our debugging after
having spent a sizeable amount of time trying to figure out what we later realized was a mismatch between our username and password for the database.
I was in the hot seat for programming today and my brain feels like it got smoother, so hoping I'll be useful for tomorrow's challenges.

February 13, 2023
Still working on explorations, I've been focused on understanding the authentication aspect. Read through the "Project Advice" that was dropped today and did the PostgreSQL database setup in "Project Setup."

February 10, 2023
Worked on knocking out explorations. Whole lotta reading together.
Finalized the API endpoints, created tables, and reconfigured the scope of our ambitious project.

February 9, 2023
So far just getting the barebones setup of our project.
Officially created our group on GitLab.
Forked over the project template and moved the API-design over.
Creating journals for everyone.

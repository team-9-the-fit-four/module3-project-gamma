steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE muscles (
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            name TEXT NOT NULL,
            date_last_used DATE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE muscles;
        """,
    ],
    [
        """
        INSERT INTO muscles (name)
        VALUES ('abdominals'), ('abductors'), ('adductors'), ('biceps'),
                ('calves'), ('chest'), ('forearms'), ('glutes'),
                ('hamstrings'), ('lats'), ('lower_back'), ('middle_back'),
                ('neck'), ('quadriceps'), ('traps'), ('triceps');
        """,
        """
        """,
    ],
]

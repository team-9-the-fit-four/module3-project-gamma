steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            first TEXT NOT NULL,
            last TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            age INTEGER,
            weight INTEGER,
            height INTEGER
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
]

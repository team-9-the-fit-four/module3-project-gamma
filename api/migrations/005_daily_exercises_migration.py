steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE daily_exercises (
            id SERIAL PRIMARY KEY,
            date DATE NOT NULL,
            muscle_id INTEGER NOT NULL,
            progress BOOLEAN NOT NULL,
            exercises JSON
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE daily_exercises;
        """,
    ],
]

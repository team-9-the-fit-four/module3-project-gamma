steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE exercises (
            id SERIAL PRIMARY KEY,
            name TEXT NOT NULL,
            type TEXT NOT NULL,
            equipment TEXT NOT NULL,
            difficulty TEXT NOT NULL,
            instructions TEXT NOT NULL,
            muscle TEXT NOT NULL,
            workout_id INTEGER NOT NULL,
            FOREIGN KEY (workout_id) REFERENCES workout (id) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE exercises;
        """,
    ],
]

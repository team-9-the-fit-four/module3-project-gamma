steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE workout (
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            user_id INTEGER NOT NULL,
            name TEXT NOT NULL,
            picture_url TEXT,
            FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE workout;
        """,
    ],
]

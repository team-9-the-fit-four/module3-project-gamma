steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE trainers (
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            name TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            phone TEXT NOT NULL,
            bio TEXT NOT NULL,
            picture_url TEXT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE trainers;
        """,
    ],
]

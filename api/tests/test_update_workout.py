from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.workouts import WorkoutRepo
from routers.workouts import WorkoutIn


client = TestClient(app)


class WorkoutRepoMock:
    workouts = []

    def get_workouts(self, user_id):
        return []

    def get_workout(self, id, user_id):
        for w in self.workouts:
            if w["id"] == id and w["user_id"] == user_id:
                return w
        return {}

    def create_workout(self, workout, user_id):
        new_workout = {
            "id": len(self.workouts) + 1,
            "user_id": user_id,
            "name": workout.name,
            "picture_url": workout.picture_url,
        }
        self.workouts.append(new_workout)
        return new_workout

    def update_workout(self, workout, user_id):
        for i, w in enumerate(self.workouts):
            if w["id"] == workout.id:
                self.workouts[i]["name"] = workout.name
                self.workouts[i]["picture_url"] = workout.picture_url
                break


def test_update_workout():
    app.dependency_overrides[WorkoutRepo] = WorkoutRepoMock
    user = {"id": 1, "username": "testuser@example.com"}
    app.dependency_overrides[
        authenticator.get_current_account_data] = lambda: user
    workout = {"name": "new name", "picture_url": "new picture"}
    response_create = client.post("/api/workouts", json=workout)
    workout_id = response_create.json()["id"]
    updated_workout = WorkoutIn(name="updated name", picture_url="new picture")
    response_update = client.put(
        f"/api/workouts/{workout_id}", json=updated_workout.dict()
    )
    assert response_update.status_code == 200
    assert response_update.json()["name"] == "updated name"
    assert response_update.json()["picture_url"] == "new picture"

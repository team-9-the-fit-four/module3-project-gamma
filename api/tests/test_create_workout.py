from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.workouts import WorkoutRepo


client = TestClient(app)


class WorkoutRepoMock:
    workouts = []

    def get_workouts(self, user_id):
        return []

    def get_workout(self, id, user_id):
        for w in self.workouts:
            if w["id"] == id and w["user_id"] == user_id:
                return w
        return {}

    def create_workout(self, workout, user_id):
        self.workouts.append(workout)
        response = {
            "name": "string",
            "picture_url": "string",
            "id": 1,
            "user_id": user_id,
        }
        response.update(workout)
        return response

    def update_workout(self, workout, user_id):
        for i, w in enumerate(self.workouts):
            if w["id"] == workout.id:
                self.workouts[i].update(workout.dict())
                break


def test_create_workout():
    app.dependency_overrides[WorkoutRepo] = WorkoutRepoMock
    user = {"id": 1, "username": "string@email.com"}
    app.dependency_overrides[
        authenticator.get_current_account_data] = lambda: user
    workout = {"name": "string", "picture_url": "string"}

    response = client.post("/api/workouts", json=workout)

    assert response.status_code == 200
    assert response.json()["name"] == "string"
    assert response.json()["picture_url"] == "string"

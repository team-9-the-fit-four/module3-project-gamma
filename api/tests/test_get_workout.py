from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from routers.workouts import WorkoutsList

client = TestClient(app)


def get_current_user_fake():
    return {
        "id": 77,
        "username": "fakeuser",
    }


class FakeWorkout:
    def get_fake_workout(self):
        return []


def test_get_workout():
    app.dependency_overrides[WorkoutsList] = FakeWorkout
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_user_fake

    response = client.get("/api/workouts/")

    assert response.status_code == 200

from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.workouts import WorkoutRepo


client = TestClient(app)


class WorkoutRepoMock:
    workouts = []

    def get_workouts(self, user_id):
        return []

    def get_workout(self, id, user_id):
        for w in self.workouts:
            if w["id"] == id and w["user_id"] == user_id:
                return w
        return {}

    def create_workout(self, workout, user_id):
        new_workout = {
            "id": len(self.workouts) + 1,
            "user_id": user_id,
            "name": workout.name,
            "picture_url": workout.picture_url,
        }
        self.workouts.append(new_workout)
        return new_workout

    def update_workout(self, workout, user_id):
        for i, w in enumerate(self.workouts):
            if w["id"] == workout.id:
                self.workouts[i]["name"] = workout.name
                self.workouts[i]["picture_url"] = workout.picture_url
                break

    def delete_workout(self, id, user_id):
        existing_workout = self.get_workout(id, user_id)
        if existing_workout is not None:
            self.workouts.remove(existing_workout)


def test_delete_workout():
    app.dependency_overrides[WorkoutRepo] = WorkoutRepoMock
    user = {"id": 1, "username": "string@email.com"}
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = lambda: user
    workout = {"id": 1, "name": "string", "picture_url": "string"}

    response = client.post("/api/workouts", json=workout)
    response = client.delete(f"/api/workouts/{workout['id']}")

    assert response.status_code == 200
    assert response.json() is True

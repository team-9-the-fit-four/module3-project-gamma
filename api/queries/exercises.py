import os
import requests

from queries.pool import pool
from typing import Optional
from pydantic import BaseModel


EXERCISES_API = os.environ.get("EXERCISES_API")


class Exercise(BaseModel):
    id: Optional[int]
    workout_id: int
    name: str
    type: str
    equipment: str
    difficulty: str
    instructions: str
    muscle: str


class ExercisesList(BaseModel):
    exercises: list[Exercise]


class ExerciseRepo:
    def api_ninjas(self, muscle: str):
        api_url = f"https://api.api-ninjas.com/v1/exercises?muscle={muscle}"
        response = requests.get(api_url, headers={"X-Api-Key": EXERCISES_API})
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            m = f"Failed to get exercises for muscle {muscle}."
            m += f"Error: {response.status_code}"
            return {
                "Error": m
            }

    def exercise_record_to_dict(self, row, description):
        exercise = None
        if row is not None:
            exercise = {}
            exercise_fields = [
                "id",
                "workout_id",
                "name",
                "type",
                "equipment",
                "difficulty",
                "instructions",
                "muscle",
            ]
            for i, column in enumerate(description):
                if column.name in exercise_fields:
                    exercise[column.name] = row[i]
        return exercise

    def get_exercises_by_workout(self, workout_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT e.id, e.workout_id, e.name,
                    e.type, e.equipment, e.difficulty,
                    e.instructions, e.muscle
                    FROM exercises e
                    INNER JOIN workout w on e.workout_id = w.id
                    INNER JOIN users u on w.user_id = u.id
                    WHERE e.workout_id = %s AND u.id = %s
                    """,
                    [workout_id, user_id],
                )
                exercises = []
                rows = cur.fetchall()
                for row in rows:
                    exercise = self.exercise_record_to_dict(
                        row, cur.description)
                    exercises.append(exercise)
                return ExercisesList(exercises=exercises)

    def create_exercise_by_workout(
        self, muscle: str, workout_id: int, exercise_index: int, user_id
    ):
        exercises = ExerciseRepo().api_ninjas(muscle)
        if "Error" in exercises:
            return exercises

        selected_exercise = exercises[exercise_index]
        exercise = Exercise(
            workout_id=workout_id,
            name=selected_exercise["name"],
            type=selected_exercise["type"],
            equipment=selected_exercise["equipment"],
            difficulty=selected_exercise["difficulty"],
            instructions=selected_exercise["instructions"],
            muscle=selected_exercise["muscle"],
        )

        with pool.connection() as conn:
            with conn.cursor() as cur:
                query = (
                    """
                    INSERT INTO exercises(name, type,
                    equipment, difficulty, instructions,
                    muscle, workout_id)
                    WHERE user_id = %s
                    VALUES(%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id
                    """,
                )
                [user_id]
                cur.execute(
                    query,
                    (
                        exercise.name,
                        exercise.type,
                        exercise.equipment,
                        exercise.difficulty,
                        exercise.instructions,
                        exercise.muscle,
                        exercise.workout_id,
                    ),
                )
                exercise.id = cur.fetchone()[0]
                conn.commit()

        return exercise

    def create_exercise(self, exercise_in, workout_id, user_id):
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT user_id FROM workout
                    WHERE id = %s
                    """,
                    [workout_id],
                )
                row = cur.fetchone()
                if not row:
                    return None
                workout_user_id = row[0]
                if workout_user_id != user_id:
                    return None

                cur.execute(
                    """
                    INSERT INTO exercises(name, type, equipment,
                    difficulty, instructions, muscle, workout_id)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        exercise_in.name,
                        exercise_in.type,
                        exercise_in.equipment,
                        exercise_in.difficulty,
                        exercise_in.instructions,
                        exercise_in.muscle,
                        workout_id,
                    ],
                )
                id = cur.fetchone()[0]
        if id is not None:
            exercise = Exercise(
                id=id,
                workout_id=workout_id,
                name=exercise_in.name,
                type=exercise_in.type,
                equipment=exercise_in.equipment,
                difficulty=exercise_in.difficulty,
                instructions=exercise_in.instructions,
                muscle=exercise_in.muscle,
            )
            return exercise

    def delete_exercise_by_workout(self, workout_id: int, id: int, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        DELETE FROM exercises
                        WHERE workout_id = %s AND id = %s AND workout_id IN(
                            SELECT id FROM workout WHERE user_id = %s
                        )
                        ;
                        """,
                    [workout_id, id, user_id],
                )
        return True

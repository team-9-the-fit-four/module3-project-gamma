from queries.pool import pool


class TrainerRepo:
    def trainer_record_to_dict(self, row, description):
        trainer = None
        if row is not None:
            trainer = {}
            trainer_fields = [
                "id",
                "name",
                "email",
                "phone",
                "bio",
                "picture_url",
            ]
            for i, column in enumerate(description):
                if column.name in trainer_fields:
                    trainer[column.name] = row[i]
        return trainer

    def get_trainers(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, email, phone, bio, picture_url
                    FROM trainers
                    ORDER BY id
                    """,
                )
                trainers = []
                rows = cur.fetchall()
                for row in rows:
                    trainer = self.trainer_record_to_dict(row, cur.description)
                    trainers.append(trainer)
                return trainers

    def get_trainer(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, email, phone, bio, picture_url
                    FROM trainers
                    WHERE id = %s
                    """,
                    [id],
                )
                row = cur.fetchone()
                return self.trainer_record_to_dict(row, cur.description)

    def create_trainer(self, trainer):
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO trainers (name, email, phone, bio, picture_url)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        trainer.name,
                        trainer.email,
                        trainer.phone,
                        trainer.bio,
                        trainer.picture_url,
                    ],
                )
                id = cur.fetchone()[0]
        if id is not None:
            return self.get_trainer(id)

    def delete_trainer(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM trainers
                    WHERE id = %s
                    """,
                    [id],
                )

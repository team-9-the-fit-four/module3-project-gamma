from pydantic import BaseModel
from queries.pool import pool
from typing import Optional


class UserIn(BaseModel):
    email: str
    password: str
    first: str
    last: str
    age: Optional[int]
    weight: Optional[int]
    height: Optional[int]


class UserOut(BaseModel):
    id: int
    email: str
    first: str
    last: str
    age: Optional[int]
    weight: Optional[int]
    height: Optional[int]

    class Config:
        orm_mode = True


class AccountOutWithPassword(UserOut):
    hashed_password: Optional[str]
    age: Optional[int]
    weight: Optional[int]
    height: Optional[int]


class UserUpdate(BaseModel):
    first: Optional[str]
    last: Optional[str]
    age: Optional[int]
    weight: Optional[int]
    height: Optional[int]


class UserRepo(BaseModel):
    def user_record_to_dict(self, row, description):
        user = None
        if row is not None:
            user = {}
            user_fields = [
                "id",
                "email",
                "first",
                "last",
                "age",
                "weight",
                "height",
                "hashed_password",
            ]
            for i, column in enumerate(description):
                if column.name in user_fields:
                    user[column.name] = row[i]
        return AccountOutWithPassword(**user)

    def get_users(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, email, first, last,
                    age, weight, height,
                    password AS hashed_password
                    FROM users
                    WHERE id = %s
                    ORDER BY id
                    """,
                    [id],
                )
                users = []
                rows = cur.fetchall()
                for row in rows:
                    user = self.user_record_to_dict(row, cur.description)
                    users.append(user)
                return users

    def get_user_by_id(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, first, last, email,
                    age, weight, height,
                    password AS hashed_password
                    FROM users
                    WHERE id = %s
                    ORDER BY last, first
                    """,
                    [id],
                )
                row = cur.fetchone()
                return self.user_record_to_dict(row, cur.description)

    def get_user_by_email(self, email):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, email, first, last,
                    age, weight, height,
                    password AS hashed_password
                    FROM users
                    WHERE email = %s
                    ORDER BY last, first
                    """,
                    [email],
                )
                row = cur.fetchone()
                return self.user_record_to_dict(row, cur.description)

    def create_user(
            self, user: UserIn, hashed_password: str
            ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO users
                    (first, last, age, weight, height, email, password)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, email, first, last,
                                age, weight, height, password;
                    """,
                    [
                        user.first,
                        user.last,
                        user.age,
                        user.weight,
                        user.height,
                        user.email,
                        hashed_password,
                    ],
                )
                row = cur.fetchone()
                if row is None:
                    return None
                return AccountOutWithPassword(
                    id=row[0],
                    email=row[1],
                    first=row[2],
                    last=row[3],
                    age=row[4],
                    weight=row[5],
                    height=row[6],
                    hashed_password=row[7],
                )

    def update_user(self, id: int, user: UserIn) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE users
                    SET first = %(first)s,
                        last = %(last)s,
                        age = %(age)s,
                        weight = %(weight)s,
                        height = %(height)s
                    WHERE id = %(id)s
                    RETURNING id, email, first, last, age, weight, height;
                    """,
                    {"id": id,
                        **user.dict(exclude_unset=True, exclude={"email"})},
                )
                row = cur.fetchone()
                if row is None:
                    return None
                return UserOut(
                    id=row[0],
                    email=row[1],
                    first=row[2],
                    last=row[3],
                    age=row[4],
                    weight=row[5],
                    height=row[6],
                )

    def delete_user(self, id: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s;
                    """,
                    [id],
                )

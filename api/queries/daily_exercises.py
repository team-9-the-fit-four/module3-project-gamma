import random
import datetime
import json
import os
import requests

from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Dict
from datetime import date
from json import JSONEncoder


EXERCISES_API = os.environ.get("EXERCISES_API")


class DateEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, date):
            return obj.isoformat()
        return super().default(obj)


class MuscleEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Muscle):
            return {
                "id": obj.id,
                "name": obj.name,
                "date_last_used": obj.date_last_used,
            }
        return super().default(obj)


class ExerciseEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Exercise):
            return {
                "id": obj.id,
                "name": obj.name,
                "type": obj.type,
                "equipment": obj.equipment,
                "difficulty": obj.difficulty,
                "instructions": obj.instructions,
                "muscle": json.loads(obj.muscle.json()),
            }
        return super().default(obj)


class Muscle(BaseModel):
    id: int
    name: str
    date_last_used: Optional[date] = None
    exercises: List = []
    __update_foward_refs__ = ["exercises"]

    def dict(self, **kwargs) -> Dict:
        return super().dict(**kwargs)


class Exercise(BaseModel):
    id: Optional[int]
    name: str
    type: str
    equipment: str
    difficulty: str
    instructions: str
    muscle: Muscle


class DailyExercises(BaseModel):
    id: int
    date: date
    muscle: Muscle
    exercises: List[Exercise]


class DailyExerciseResponse(BaseModel):
    id: int
    date: date
    muscle: Muscle
    exercises: List[Exercise]


def generate_daily_exercises() -> DailyExercises:
    today = datetime.date.today()
    with pool.getconn() as conn:
        with conn.cursor() as cur:
            today_str = today.strftime("%Y-%m-%d")
            cur.execute(
                """
                SELECT *
                FROM daily_exercises
                WHERE date = %s
                """, (today_str,))

            row = cur.fetchone()

            if row:
                daily_exercises_id = row[0]
                muscle_id = row[2]
                exercises = row[4]
                if isinstance(exercises, list):
                    exercises = json.dumps(exercises, cls=MuscleEncoder)
                cur.execute(
                    """
                    SELECT *
                    FROM muscles
                    WHERE id = %s
                    """, (muscle_id,))
                row = cur.fetchone()
                if row:
                    if row[2]:
                        muscle_date_last_used = row[2].isoformat()
                    else:
                        muscle_date_last_used = None
                    muscle = Muscle(
                        id=row[0],
                        name=row[1],
                        date_last_used=muscle_date_last_used
                    )
                else:
                    raise ValueError(f"No muscle found for id {muscle_id}")
                existing_daily_exercises = DailyExercises(
                    id=daily_exercises_id,
                    date=today,
                    muscle=muscle,
                    exercises=json.loads(exercises),
                )
                return existing_daily_exercises
            else:
                cur.execute(
                    """
                    SELECT id, name, date_last_used
                    FROM muscles
                    WHERE date_last_used IS NULL
                    OR date_last_used < %s
                    ORDER BY random() LIMIT 1
                    """,
                    (today,),
                )
                row = cur.fetchone()
                if row:
                    muscle_id = row[0]
                    cur.execute(
                        """
                        UPDATE muscles
                        SET date_last_used = %s
                        WHERE id = %s
                        """,
                        (
                            today,
                            muscle_id,
                        ),
                    )
                else:
                    cur.execute(
                        """
                        SELECT id, name, date_last_used
                        FROM muscles WHERE date_last_used IS NOT NULL
                        ORDER BY date_last_used ASC LIMIT 1
                        """
                    )
                    row = cur.fetchone()
                    muscle_id = row[0]

                muscle = Muscle(id=row[0], name=row[1], date_last_used=row[2])

                cur.execute(
                    """
                    SELECT date_last_used
                    FROM muscles
                    WHERE id = %s
                    """, (muscle_id,)
                )
                row = cur.fetchone()
                if not row[0]:
                    cur.execute(
                        "UPDATE muscles SET date_last_used = %s WHERE id = %s",
                        (
                            today,
                            muscle_id,
                        ),
                    )

                api_url = (
                    "https://api.api-ninjas.com/v1/exercises"
                )
                params = {"muscle": muscle.name}
                response = requests.get(
                    api_url,
                    headers={"X-Api-Key": EXERCISES_API},
                    params=params)
                if response.status_code == requests.codes.ok:
                    exercises = response.json()
                    if exercises:
                        selected_exercises = random.sample(
                            exercises, min(5, len(exercises))
                        )
                    else:
                        selected_exercises = []
                else:
                    exercises = []
                    selected_exercises = []

                exercise_models = []
                for exercise in selected_exercises:
                    exercise_model = Exercise(
                        id=None,
                        name=exercise["name"],
                        type=exercise["type"],
                        equipment=exercise["equipment"],
                        difficulty=exercise["difficulty"],
                        instructions=exercise["instructions"],
                        muscle=muscle,
                    )
                    exercise_models.append(exercise_model)

                cur.execute(
                    """
                    INSERT INTO daily_exercises
                    (date, muscle_id, progress, exercises)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id
                    """,
                    (
                        today,
                        muscle_id,
                        False,
                        json.dumps(exercise_models, cls=ExerciseEncoder),
                    ),
                )
                row = cur.fetchone()
                daily_exercises_id = row[0]
                conn.commit()

                daily_exercises = DailyExercises(
                    id=daily_exercises_id,
                    date=today,
                    muscle=muscle,
                    exercises=exercise_models,
                )
                return daily_exercises

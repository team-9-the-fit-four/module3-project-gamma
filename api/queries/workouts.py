from queries.pool import pool


class WorkoutRepo:
    def workout_record_to_dict(self, row, description):
        workout = None
        if row is not None:
            workout = {}
            workout_fields = [
                "id",
                "user_id",
                "name",
                "picture_url",
            ]
            for i, column in enumerate(description):
                if column.name in workout_fields:
                    workout[column.name] = row[i]
        return workout

    def get_workouts(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, user_id, name, picture_url
                    FROM workout
                    WHERE user_id = %s
                    """,
                    (user_id,),
                )
                workouts = []
                rows = cur.fetchall()
                for row in rows:
                    workout = self.workout_record_to_dict(row, cur.description)
                    workouts.append(workout)
                return workouts

    def get_workout(self, id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, user_id, name, picture_url
                    FROM workout
                    WHERE id = %s AND user_id = %s
                    """,
                    [id, user_id],
                )
                row = cur.fetchone()
                return self.workout_record_to_dict(row, cur.description)

    def create_workout(self, workout, user_id):
        id = None
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO workout (user_id, name, picture_url)
                    VALUES (%s, %s, %s)
                    RETURNING id;
                    """,
                    [user_id, workout.name, workout.picture_url],
                )
                id = cur.fetchone()[0]
        if id is not None:
            return self.get_workout(id, user_id)

    def update_workout(self, workout, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE workout
                    SET name = %s,
                        picture_url = %s
                    WHERE id = %s AND user_id = %s
                    """,
                    [workout.name, workout.picture_url, workout.id, user_id],
                )

    def delete_workout(self, id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM workout
                    WHERE id = %s AND user_id =%s
                    """,
                    [id, user_id],
                )

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
from queries.exercises import ExerciseRepo
from authenticator import authenticator


class ApiNinjas(BaseModel):
    id: int
    name: str
    type: str
    equipment: str
    difficulty: str
    instructions: str
    muscle: str


class ExerciseIn(BaseModel):
    name: str
    type: str
    equipment: str
    difficulty: str
    instructions: str
    muscle: str


class Exercise(ExerciseIn):
    id: int
    workout_id: int


class ExercisesList(BaseModel):
    exercises: list[Exercise]


class ExerciseCreateRequest(BaseModel):
    muscle: str
    exercise_index: int


router = APIRouter()


@router.get("/api/exercises/{muscle}")
def api_ninjas(muscle: str, repo: ExerciseRepo = Depends()):
    return repo.api_ninjas(muscle)


@router.get(
        "/api/workouts/{workout_id}/exercises",
        response_model=ExercisesList)
def get_exercises_by_workout(
    workout_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: ExerciseRepo = Depends(),
):
    return repo.get_exercises_by_workout(workout_id, account_data.get("id"))


@router.post("/api/workouts/{workout_id}/exercises", response_model=Exercise)
def create_exercise(
    workout_id: int,
    exercise_in: ExerciseIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: ExerciseRepo = Depends(),
):
    exercise = repo.create_exercise(
        exercise_in=exercise_in,
        workout_id=workout_id,
        user_id=account_data.get("id")
    )
    if exercise is None:
        raise HTTPException(status_code=404, detail="Couldn't create exercise")
    return exercise


@router.delete(
        "/api/workouts/{workout_id}/exercises/{id}",
        response_model=bool)
def delete_exercise_by_workout(
    workout_id: int,
    id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: ExerciseRepo = Depends(),
):
    return repo.delete_exercise_by_workout(
        workout_id,
        id,
        account_data.get("id")
        )

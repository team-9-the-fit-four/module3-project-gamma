from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
from typing import Optional
from authenticator import authenticator
from queries.workouts import WorkoutRepo

router = APIRouter()


class Workout(BaseModel):
    id: int
    user_id: int
    name: str
    picture_url: Optional[str]


class WorkoutIn(BaseModel):
    name: str
    picture_url: Optional[str]


class WorkoutOut(WorkoutIn):
    id: int
    user_id: int


class WorkoutsList(BaseModel):
    workouts: list[WorkoutOut]


@router.post("/api/workouts", response_model=WorkoutOut)
def create_workout(
    workout: WorkoutIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: WorkoutRepo = Depends(),
):
    return repo.create_workout(workout, account_data.get("id"))


@router.get("/api/workouts", response_model=WorkoutsList)
def get_workouts(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: WorkoutRepo = Depends(),
):
    return {"workouts": repo.get_workouts(account_data.get("id"))}


@router.get("/api/workouts/{id}", response_model=WorkoutOut)
def get_workout(
    id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: WorkoutRepo = Depends(),
):
    workout = repo.get_workout(id, account_data.get("id"))
    if workout:
        return workout
    else:
        raise HTTPException(status_code=404, detail="Workout not found")


@router.put("/api/workouts/{id}", response_model=WorkoutOut)
def update_workout(
    id: int,
    workout: WorkoutIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: WorkoutRepo = Depends(),
):
    user_id = account_data.get("id")
    existing_workout_dict = repo.get_workout(id, user_id)
    if workout is None:
        raise HTTPException(status_code=404, detail="Workout not found")
    else:
        existing_workout = Workout(**existing_workout_dict)
        updated_workout = WorkoutOut(
            id=existing_workout.id,
            user_id=existing_workout.user_id,
            name=workout.name or existing_workout.name,
            picture_url=workout.picture_url or existing_workout.picture_url,
        )
        repo.update_workout(updated_workout, user_id)
        return updated_workout


@router.delete("/api/workouts/{id}", response_model=bool)
def delete_workout(
    id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: WorkoutRepo = Depends(),
):
    repo.delete_workout(id, account_data.get("id"))
    return True

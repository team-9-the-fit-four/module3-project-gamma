from fastapi import APIRouter


from queries.daily_exercises import generate_daily_exercises
from queries.daily_exercises import DailyExerciseResponse

router = APIRouter()


@router.get("/daily_exercises")
async def get_daily_exercises():
    daily_exercises = generate_daily_exercises()
    response = DailyExerciseResponse(
        id=daily_exercises.id,
        date=daily_exercises.date,
        muscle=daily_exercises.muscle,
        exercises=daily_exercises.exercises,
    )
    return response

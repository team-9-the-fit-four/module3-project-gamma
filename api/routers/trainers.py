from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional
from queries.trainers import TrainerRepo


router = APIRouter()


class Trainer(BaseModel):
    id: int
    name: str
    email: str
    phone: str
    bio: str
    picture_url: Optional[str]


class TrainerIn(BaseModel):
    name: str
    email: str
    phone: str
    bio: str
    picture_url: Optional[str]


class TrainerOut(TrainerIn):
    id: int


class TrainersList(BaseModel):
    trainers: list[TrainerOut]


@router.post("/api/trainers", response_model=TrainerOut)
def create_trainer(
    trainer: TrainerIn,
    repo: TrainerRepo = Depends(),
):
    return repo.create_trainer(trainer)


@router.get("/api/trainers", response_model=TrainersList)
def get_trainers(
    repo: TrainerRepo = Depends(),
):
    return {"trainers": repo.get_trainers()}


@router.delete("/api/trainers/{id}", response_model=bool)
def delete_trainer(
    id: int,
    repo: TrainerRepo = Depends(),
):
    repo.delete_trainer(id)
    return True

## Household Hustle

* John Vanderpuye
* Keyan Rogalsky
* Gabby Tietjen
* Kenny Zheng

Household Hustle – Daily workouts for beginners to avid gym goers.

Household Hustle - a fun starting place.

Household Hustle - Create and Save your own workouts.

## Design

* [API design](https://gitlab.com/team-9-the-fit-four/module3-project-gamma/-/tree/main/api)
* [Tables](https://gitlab.com/team-9-the-fit-four/module3-project-gamma/-/tree/main/api/migrations)
* [GHI](https://gitlab.com/team-9-the-fit-four/module3-project-gamma/-/tree/main/ghi)
* [Integrations](https://gitlab.com/team-9-the-fit-four/module3-project-gamma/-/blob/main/docs/integration.md)


## Intended market
Our target audience casts a wide net.  We are appealing to people who do not know where to start, while also marketing to people who regularly go to the gym.  Any one who wants to workout,
this application is for them.

## Functionality

* Visitors who come to the site will be greeted with a daily workout, no matter if your are signed in or not.
* This is to make people feel comfortable going to the site with no pressure.
* If you want to take advantage of all the site offers by signing up, you can create a workout and save them.
* You can also keep track of your weight in your profile.
* All of this will keep visitors coming back to the site to reuse different functionality or create new workouts.
* You can delete workouts you saved if you feel they no longer pertain to where you are at fitness wise.
* You can also edit any workout you've created.
* You can mix and match muscle groups to create your own unique workout to fit your needs.

In addition to all things workout, we have created a fun environment for the user.  With the neon color scheme to get the user pumped, in addition to the music player
at the top of each screen.




## Project Initialization
To run this application on your local machine, please make sure to follow these steps:

* Clone the repository down to your local machine

* CD into the new project directory

* Run `docker volume create pg-admin`

* Run `docker volume create postgres-data`

* Run `docker compose build`

* Run `docker compose up`

Have a nice workout with Household Hustle!

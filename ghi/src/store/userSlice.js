import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  loggedin: false,
  show: null,
  username: "",
  password: "",
  first: "",
  last: "",
  age: "",
  weight: "",
  height: "",
};


export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    updateField: (state, action) => {
      state[action.payload.field] = action.payload.value;
    },


    showModal: (state, action) => {
      state.show = action.payload;
    },


    clearForm: (state) => {
      state.show=null
      state.username = ""
      state.password = ""
      state.first = ""
      state.last = ""
      state.age = ""
      state.weight = ""
      state.height = ""
    },


    updateLoggedIn: (state)=>{
      state.loggedin = !state.loggedin;
    }
  },
});


export const { clearForm, updateField, showModal, updateLoggedIn } = userSlice.actions;

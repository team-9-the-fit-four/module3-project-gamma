import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { authApiSlice } from "./authApi";
import { userSlice } from "./userSlice";
import { workoutApi } from "./workoutApi";
import { workoutSlice } from "./workoutSlice";


export const store = configureStore({
  reducer: {
    [workoutApi.reducerPath]: workoutApi.reducer,
    [userSlice.name]: userSlice.reducer,
    [authApiSlice.reducerPath]: authApiSlice.reducer,
    workoutForm: workoutSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(workoutApi.middleware)
      .concat(authApiSlice.middleware),
});


setupListeners(store.dispatch);

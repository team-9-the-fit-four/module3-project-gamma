import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  workoutName: "",
  pictureUrl: "",
};


export const workoutSlice = createSlice({
  name: "workoutForm",
  initialState,
  reducers: {
    setWorkoutName: (state, action) => {
      state.workoutName = action.payload;
    },


    setWorkoutPictureUrl: (state, action) => {
      state.pictureUrl = action.payload;
    },


    clearWorkoutForm: (state) => {
      state.workoutName = "";
      state.pictureUrl = "";
    },
  },
});


export const { setWorkoutName, setWorkoutPictureUrl, clearWorkoutForm } =
  workoutSlice.actions;

import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { clearForm, updateLoggedIn } from "./userSlice";


export const authApiSlice = createApi({
  reducerPath: "users",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SAMPLE_SERVICE_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = authApiSlice.endpoints.getToken.select();
      const { data: tokenData } = selector(getState());
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      return headers;
    },
  }),


  tagTypes: ["User", "Token", "Workouts", "Exercises"],
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (data) => ({
        url: "/api/users",
        method: "post",
        body: data,
        credentials: "include",
      }),
      providesTags: ["User"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),


    logIn: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("email", info.email);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      providesTags: ["User"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(updateLoggedIn());
          dispatch(clearForm());
        } catch (err) {}
      },
    }),


    logOut: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["User", "Token", "Workouts", "Exercises"],
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(updateLoggedIn());
        } catch (err) {}
      },
    }),


    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["Token"],
    }),


    generateDailyExercises: builder.query({
      query: () => ({
        url: "/daily_exercises",
        method: "get",
        credentials: "include",
      }),
      providesTags: ["DailyExercises"],
    }),


    getUserWorkouts: builder.query({
      query: () => ({
        url: "/api/workouts",
        credentials: "include",
      }),
      providesTags: [
        (data) => {
          const tags = [{ type: "Workouts", id: "LIST" }];
          if (!data || !data.workouts) return tags;

          const { workouts } = data;
          if (workouts) {
            tags.concat(
              ...workouts.map(({ id }) => ({ type: "Workouts", id }))
            );
          }
          return tags;
        },
      ],
    }),


    getUserWorkoutExercises: builder.query({
      query: (workout_id) => ({
        url: `/api/workouts/${workout_id}/exercises`,
        credentials: "include",
      }),
      providesTags: [
        (data) => {
          const tags = [{ type: "Exercises", id: "LIST" }];
          if (!data || !data.exercises) return tags;

          const { exercises } = data;
          if (exercises) {
            tags.concat(
              ...exercises.map(({ id }) => ({ type: "Exercises", id }))
            );
          }
          return tags;
        },
      ],
    }),


    createWorkout: builder.mutation({
      query: (workout) => ({
        url: "/api/workouts",
        method: "post",
        body: workout,
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),


    updateWorkout: builder.mutation({
      query: ({ id, name }) => ({
        url: `/api/workouts/${id}`,
        method: "put",
        body: { name },
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),


    updateUser: builder.mutation({
      query: ({ user_id, data }) => ({
        url: `/api/users/${user_id}`,
        method: "put",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["User"],
    }),


    deleteWorkout: builder.mutation({
      query: (id) => ({
        url: `/api/workouts/${id}`,
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),


    apiNinjas: builder.query({
      query: (muscle) => ({
        url: `/api/exercises/${muscle}`,
      }),
      providesTags: (result, error, muscle) => [
        { type: "Exercises", id: muscle },
      ],
    }),


    createExercise: builder.mutation({
      query: ({ workout_id, exercise }) => ({
        url: `/api/workouts/${workout_id}/exercises`,
        method: "post",
        body: exercise,
        credentials: "include",
      }),
      invalidatesTags: ["Exercises"],
    }),


    deleteExercise: builder.mutation({
      query: ({ workout_id, id }) => ({
        url: `/api/workouts/${workout_id}/exercises/${id}`,
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["Exercises"],
    }),


    getUserProfile: builder.query({
      query: () => ({
        url: "/api/users",
        credentials: "include",
      }),
      providesTags: [
        { type: "Users", id: "LIST" },
        (result) => ({ type: "Users", id: result?.id }),
      ],
    }),
  }),
});


export const {
  useGetTokenQuery,
  useLogInMutation,
  useLogOutMutation,
  useSignUpMutation,
  useGetUserWorkoutsQuery,
  useGetUserWorkoutExercisesQuery,
  useGenerateDailyExercisesQuery,
  useCreateWorkoutMutation,
  useUpdateWorkoutMutation,
  useCreateExerciseMutation,
  useDeleteWorkoutMutation,
  useApiNinjasQuery,
  useDeleteExerciseMutation,
  useGetUserProfileQuery,
  useUpdateUserMutation,
} = authApiSlice;

import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  name: "",
  type: "",
  difficulty: "",
  instructions: "",
  muscle: "",
};


export const exerciseSlice = createSlice({
  name: "exerciseForm",
  initialState,
  reducers: {
    setExerciseForm: (state, action) => {
      state.name = action.payload.name;
      state.type = action.payload.type;
      state.difficulty = action.payload.difficulty;
      state.instructions = action.payload.instructions;
      state.muscle = action.payload.muscle;
    },
    clearExerciseForm: (state) => {
      state.name = "";
      state.type = "";
      state.difficulty = "";
      state.instructions = "";
      state.muscle = "";
    },
  },
});


export const { setExerciseForm, clearExerciseForm } = exerciseSlice.actions;

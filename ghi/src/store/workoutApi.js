import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { authApiSlice } from "./authApi";


export const workoutApi = createApi({
  reducerPath: "workouts",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SAMPLE_SERVICE_API_HOST,
    prepareHeaders: async (headers) => {
      const tokenData = authApiSlice.util.getQueryArgs("getToken")(
        authApiSlice.getState()
      );
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      headers.set("Content-Type", "application/json");
      return headers;
    },
  }),


  endpoints: (builder) => ({
    getWorkout: builder.query({
      query: () => "/workouts",
    }),


    addNewWorkout: builder.mutation({
      query: (data) => ({
        url: "/api/workouts",
        method: "post",
        body: JSON.stringify(data),
      }),
    }),
  }),
});


export const { useGetworkoutQuery, useAddNewWorkoutMutation } = workoutApi;

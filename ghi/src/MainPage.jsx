import React, { useState } from "react";
import { useGenerateDailyExercisesQuery } from "./store/authApi";
import "./style/MainPage.css";
import "./style/RocketButton.css";

import * as THREE from "three";


function MainPage() {
  const { data: dailyExercises, isLoading } = useGenerateDailyExercisesQuery();
  const [showInstructions, setShowInstructions] = useState(false);
  const [showBold, setShowBold] = useState(false);

  const handleTryItOutClick = () => {
    setShowInstructions(true);
    setShowBold(true);
    document
      .querySelector(".main-page-card")
      .classList.add("main-page-card--fit");
    document.querySelectorAll(".welcome-text").forEach((el) => {
      el.classList.add("hidden");
    });
  };

  const handleHideInstructionsClick = () => {
    setShowInstructions(false);
    setShowBold(false);
    document
      .querySelector(".main-page-card")
      .classList.remove("main-page-card--fit");
    document.querySelectorAll(".welcome-text").forEach((el) => {
      el.classList.remove("hidden");
    });
  };

  return (
    <div className="home-page">
      <h1 className="welcome-text">⚡Welcome to Household Hustle!⚡</h1>
      <h2 className="welcome-text">💪Today's Workout💪</h2>
      {isLoading && <p>Loading daily exercise...</p>}
      {dailyExercises && (
        <div className="main-page-card">
          <h3 style={{ fontWeight: "bold", textDecoration: "underline" }}>
            {dailyExercises.muscle.name.toUpperCase()} EXERCISES
          </h3>
          <ul className="exercise-list">
            {dailyExercises.exercises.map((exercise) => (
              <li key={exercise.name}>
                <div className="exercise-info">
                  <span>
                    {showBold ? (
                      <b>
                        {exercise.name} ({exercise.type}, {exercise.equipment})
                      </b>
                    ) : (
                      `${exercise.name} (${exercise.type}, ${exercise.equipment})`
                    )}
                  </span>
                </div>
                {showInstructions && <p>{exercise.instructions}</p>}
              </li>
            ))}
          </ul>
          {!showInstructions && (
            <div id="pulsa" className="pulsa">
              <div className="vertical-spacer-rocket"></div>
              <button className="cta" id="cta" onClick={handleTryItOutClick}>
                TRY IT OUT
                <div className="star a"></div>
                <div className="star b"></div>
                <div className="star c"></div>
                <div className="star d"></div>
                <div className="star e"></div>
                <div className="star f"></div>
                <div className="star g"></div>
                <svg
                  version="1.1"
                  viewBox="0 0 1024 1024"
                  width="24px"
                  className="rocket"
                >
                  <path
                    fill="#3A7EB9"
                    d="M662.72 462.784l136.448 169.408v173.248l-136.448-48.32zM342.72 457.344L206.272 626.816v173.248l136.448-48.384z"
                  ></path>
                  <path
                    fill="#D48171"
                    d="M570.688 418.688l-142.848 1.152a266.496 266.496 0 0 1-20.288-0.576l3.712 448.64c0.256 28.928 94.272 130.048 94.272 130.048s93.888-102.656 93.632-131.584l-3.712-448.96a344.64 344.64 0 0 1-24.768 1.28z"
                  ></path>
                  <path
                    fill="#E9DF92"
                    d="M531.456 599.296l-63.04 0.576c-3.008 0-5.952-0.064-8.96-0.384l2.176 257.792c0.128 16.64 41.728 74.816 41.728 74.816s41.344-58.944 41.28-75.52l-2.176-257.92c-3.648 0.384-7.296 0.64-11.008 0.64z"
                  ></path>
                  <path
                    fill="#B5D5EB"
                    d="M554.304 93.568a324.352 324.352 0 0 0-110.592 1.728L342.72 240.768v584.512c13.824-0.96 27.968-1.536 42.368-1.536h245.248c11.84 0 23.36 0.384 34.816 1.024V253.312L554.304 93.568z"
                  ></path>
                  <path
                    fill="#3A7EB9"
                    d="M541.44 94.144L500.416 35.008l-45.696 59.136v29.504h89.024v-29.504zM459.456 288.64h88.96v88.896h-88.96zM459.456 467.456h88.96v88.96h-88.96zM459.456 634.176h88.96v88.896h-88.96zM364.928 788.736h277.76v44.352h-277.76z"
                  ></path>
                </svg>
              </button>
            </div>
          )}
          {showInstructions && (
            <div id="pulsa" className="pulsa">
              <div className="vertical-spacer-rocket"></div>
              <button
                className="cta"
                id="cta"
                onClick={handleHideInstructionsClick}
              >
                Hide Instructions
                <div className="star a"></div>
                <div className="star b"></div>
                <div className="star c"></div>
                <div className="star d"></div>
                <div className="star e"></div>
                <div className="star f"></div>
                <div className="star g"></div>
                <svg
                  version="1.1"
                  viewBox="0 0 1024 1024"
                  width="24px"
                  className="rocket"
                >
                  <path
                    fill="#3A7EB9"
                    d="M662.72 462.784l136.448 169.408v173.248l-136.448-48.32zM342.72 457.344L206.272 626.816v173.248l136.448-48.384z"
                  ></path>
                  <path
                    fill="#D48171"
                    d="M570.688 418.688l-142.848 1.152a266.496 266.496 0 0 1-20.288-0.576l3.712 448.64c0.256 28.928 94.272 130.048 94.272 130.048s93.888-102.656 93.632-131.584l-3.712-448.96a344.64 344.64 0 0 1-24.768 1.28z"
                  ></path>
                  <path
                    fill="#E9DF92"
                    d="M531.456 599.296l-63.04 0.576c-3.008 0-5.952-0.064-8.96-0.384l2.176 257.792c0.128 16.64 41.728 74.816 41.728 74.816s41.344-58.944 41.28-75.52l-2.176-257.92c-3.648 0.384-7.296 0.64-11.008 0.64z"
                  ></path>
                  <path
                    fill="#B5D5EB"
                    d="M554.304 93.568a324.352 324.352 0 0 0-110.592 1.728L342.72 240.768v584.512c13.824-0.96 27.968-1.536 42.368-1.536h245.248c11.84 0 23.36 0.384 34.816 1.024V253.312L554.304 93.568z"
                  ></path>
                  <path
                    fill="#3A7EB9"
                    d="M541.44 94.144L500.416 35.008l-45.696 59.136v29.504h89.024v-29.504zM459.456 288.64h88.96v88.896h-88.96zM459.456 467.456h88.96v88.96h-88.96zM459.456 634.176h88.96v88.896h-88.96zM364.928 788.736h277.76v44.352h-277.76z"
                  ></path>
                </svg>
              </button>
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default MainPage;


let scene,
  camera,
  renderer,
  cloudParticles = [],
  flash,
  rain,
  rainGeo,
  rainCount = 15000,
  ambient,
  directionalLight,
  rainMaterial,
  cloudGeo,
  cloudMaterial;

function init() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(
    60,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  camera.position.z = 1;
  camera.rotation.x = 1.16;
  camera.rotation.y = -0.12;
  camera.rotation.z = 0.27;

  ambient = new THREE.AmbientLight(0x555555);
  scene.add(ambient);

  directionalLight = new THREE.DirectionalLight(0xffeedd);
  directionalLight.position.set(0, 0, 1);
  scene.add(directionalLight);

  flash = new THREE.PointLight(0x062d89, 30, 500, 1.7);
  flash.position.set(200, 300, 100);
  scene.add(flash);

  renderer = new THREE.WebGLRenderer();

  scene.fog = new THREE.FogExp2(0x11111f, 0.002);
  renderer.setClearColor(scene.fog.color);

  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  let positions = [];
  let sizes = [];
  rainGeo = new THREE.BufferGeometry();
  for (let i = 0; i < rainCount; i++) {
    positions.push(Math.random() * 400 - 200);
    positions.push(Math.random() * 500 - 250);
    positions.push(Math.random() * 400 - 200);
    sizes.push(30);
  }
  rainGeo.setAttribute(
    "position",
    new THREE.BufferAttribute(new Float32Array(positions), 3)
  );
  rainGeo.setAttribute(
    "size",
    new THREE.BufferAttribute(new Float32Array(sizes), 1)
  );
  rainMaterial = new THREE.PointsMaterial({
    color: 0xaaaaaa,
    size: 0.1,
    transparent: true,
  });
  rain = new THREE.Points(rainGeo, rainMaterial);
  scene.add(rain);

  let loader = new THREE.TextureLoader();
  loader.load(
    "https://static.vecteezy.com/system/resources/previews/010/884/548/original/dense-fluffy-puffs-of-white-smoke-and-fog-on-transparent-background-abstract-smoke-clouds-movement-blurred-out-of-focus-smoking-blows-from-machine-dry-ice-fly-fluttering-in-air-effect-texture-png.png",
    function (texture) {
      cloudGeo = new THREE.PlaneGeometry(500, 500);
      cloudMaterial = new THREE.MeshLambertMaterial({
        map: texture,
        transparent: true,
      });

      for (let p = 0; p < 25; p++) {
        let cloud = new THREE.Mesh(cloudGeo, cloudMaterial);
        cloud.position.set(
          Math.random() * 800 - 400,
          500,
          Math.random() * 500 - 450
        );
        cloud.rotation.x = 1.16;
        cloud.rotation.y = -0.12;
        cloud.rotation.z = Math.random() * 360;
        cloud.material.opacity = 0.6;
        cloudParticles.push(cloud);
        scene.add(cloud);
      }
      animate();
      window.addEventListener("resize", onWindowResize);
    }
  );
}

function animate() {
  cloudParticles.forEach((p) => {
    p.rotation.z -= 0.002;
  });
  rainGeo.attributes.size.array.forEach((r) => {
    r += 0.3;
  });

  // const time = Date.now() * 0.005;

  rainGeo.verticesNeedUpdate = true;
  rain.position.z -= 0.222;

  if (rain.position.z < -200) {
    rain.position.z = 0;
  }

  if (Math.random() > 0.93 || flash.power > 100) {
    if (flash.power < 100)
      flash.position.set(Math.random() * 400, 300 + Math.random() * 200, 100);
    flash.power = 50 + Math.random() * 500;
  }
  renderer.render(scene, camera);
  requestAnimationFrame(animate);
}

init();

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useLogOutMutation } from "./store/authApi";
import { useEffect } from "react";
import Circlelogo from "./logo.png";
import { GiHamburgerMenu } from "react-icons/gi";
import Dropdown from "react-bootstrap/Dropdown";
import { useSelector } from "react-redux";
import Music from "./Music";
import toast, { Toaster } from "react-hot-toast";


function Nav() {
  const navigate = useNavigate();
  const [logOut, { data }] = useLogOutMutation();
  const user = useSelector((state) => state.user);


  const handleLogout = () => {
    toast.success("Logout Successful!");
    setTimeout(() => {
      logOut();
      navigate("/");
    }, 2000);
  };


  useEffect(() => {
    if (data) {
      navigate("/login");
    }
  }, [data, navigate]);

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <Toaster />
      <div className="container-fluid">
        <div className="d-flex align-items-center">
          <NavLink className="navbar-brand" to="/">
            <img src={Circlelogo} alt="logo" className="logo" />
          </NavLink>
          <Music />
        </div>
        <Dropdown>
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            <GiHamburgerMenu />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {!user.loggedin ? (
              <>
                <Dropdown.Item href="/login">Login</Dropdown.Item>
                <Dropdown.Item href="/signup">Sign up</Dropdown.Item>
                <Dropdown.Item as={NavLink} to="/trainers">
                  Trainers
                </Dropdown.Item>
              </>
            ) : (
              <>
                <Dropdown.Item onClick={handleLogout}>Log out</Dropdown.Item>
                <Dropdown.Item as={NavLink} to="/profile">
                  Profile
                </Dropdown.Item>
                <Dropdown.Item as={NavLink} to="/workouts">
                  Workouts
                </Dropdown.Item>
                <Dropdown.Item as={NavLink} to="/trainers">
                  Trainers
                </Dropdown.Item>
              </>
            )}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </nav>
  );
}

export default Nav;

import { useState, useEffect, useCallback } from "react";
import "../style/MainPage.css";
import "../style/Trainer.css"


function TrainerList() {
  const [trainers, setTrainers] = useState([]);
  const baseUrl = process.env.REACT_APP_SAMPLE_SERVICE_API_HOST;

  const getData = useCallback(async () => {
      const response = await fetch(`${baseUrl}/api/trainers`);
      if (response.ok) {
        const data = await response.json();
        setTrainers(data.trainers);
      }
    }, [baseUrl]);

  useEffect(() => {
    getData();
  }, [getData]);

  return (
    <div className="container">
      <div className="row row-cols-1 row-cols-md-2 row-cols-lg-4 g-4">
        {trainers.map((trainer) => (
          <div className="col" key={trainer.id}>
            <div className="card trainer-card">
              <img
                src={trainer.picture_url}
                className="card-img-top trainer-img"
                alt={`${trainer.name}'s profile`}
              />
              <div className="card-body">
                <h5 className="card-title">{trainer.name}</h5>
                <p className="card-text">{trainer.email}</p>
                <p className="card-text">{trainer.phone}</p>
                <p className="card-text">{trainer.bio}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}


export default TrainerList;

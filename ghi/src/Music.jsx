import React, { useRef } from "react";
import phonk from "./phonk.mp3";
import "./style/Music.css"


function Music() {
  const audioRef = useRef(null);

  
  const handleVolumeChange = (e) => {
    audioRef.current.volume = e.target.value;
  };

  return (
    <div>
      <audio ref={audioRef} src={phonk} autoPlay loop>
        <p>Your browser does not support the audio element.</p>
      </audio>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
        }}
      >
        <button
          className="btn-space"
          onClick={() => audioRef.current.play()}
        >
          <strong className="strong-space">🎵Play Music🎵</strong>
          <div id="container-stars">
            <div id="stars"></div>
          </div>
          <div id="glow">
            <div className="circle"></div>
            <div className="circle"></div>
          </div>
        </button>
        <div className="vertical-spacer-slider"></div>
        <button
          className="btn-space"
          onClick={() => audioRef.current.pause()}
        >
          <strong className="strong-space">⏸Pause⏸</strong>
          <div id="container-stars">
            <div id="stars"></div>
          </div>
          <div id="glow">
            <div className="circle"></div>
            <div className="circle"></div>
          </div>
        </button>
        <div className="vertical-spacer-slider"></div>
        <input
          type="range"
          min="0"
          max="1"
          step="0.05"
          onChange={handleVolumeChange}
        />
      </div>
    </div>
  );
}

export default Music;

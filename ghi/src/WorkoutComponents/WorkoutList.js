import { useState } from "react";
import {
  useGetUserWorkoutsQuery,
  useUpdateWorkoutMutation,
  useDeleteWorkoutMutation,
} from "../store/authApi";
import { Link } from "react-router-dom";
import toast from "react-hot-toast";
import "../style/WorkoutList.css"


function WorkoutList() {
  const { data, isLoading, refetch } = useGetUserWorkoutsQuery();
  const [editableWorkout, setEditableWorkout] = useState(null);
  const [updateWorkout] = useUpdateWorkoutMutation();
  const [deleteWorkout] = useDeleteWorkoutMutation();
  const [workouts, setWorkouts] = useState(data?.workouts || []);
  const handleNameChange = (event, workout) => {
    const updatedWorkouts = workouts.map((w) => {
      if (w.id === workout.id) {
        return { ...w, name: event.target.value };
      } else {
        return w;
      }
    });
    setWorkouts(updatedWorkouts);
    setEditableWorkout({ ...editableWorkout, name: event.target.value });
  };


  const handleInputBlur = () => {
    if (editableWorkout) {
    }
  };


  const handleSaveClick = async () => {
    await updateWorkout({
      id: editableWorkout.id,
      name: editableWorkout.name,
    }).unwrap();
    setEditableWorkout(null);
    const { data: updatedWorkouts } = await refetch();
    setWorkouts(updatedWorkouts.workouts);
  };


  const handleDeleteClick = async (workout) => {
    if (
      window.confirm(
        `Are you sure you want to delete the workout "${workout.name}"?`
      )
    ) {
      toast.success('Workout Delete Successful!');
      await deleteWorkout(workout.id).unwrap();
      const updatedWorkouts = workouts.filter((w) => w.id !== workout.id);
      setWorkouts(updatedWorkouts);
      await refetch();
    }
  };


  if (isLoading) {
    return (
      <progress className="progress is-primary" max="100">
        Loading
      </progress>
    );
  } else {
    return (
      <div className="container">
        <Link to="/workouts/new" className="btn btn-primary mb-3">
          Create New Workout
        </Link>
        <div className="row row-cols-1 row-cols-md-3 g-4">
          {data?.workouts.map((workout) => {
            if (editableWorkout && editableWorkout.id === workout.id) {
              return (
                <div className="col" key={workout.id}>
                  <div className="card workout-card">
                    <div className="card-body">
                      <Link
                        to={{
                          pathname: `/workouts/${workout.id}/exercises`,
                        }}
                        className="card-title"
                      >
                        <h1 style={{ color: "blue" }}>{workout.name}</h1>
                      </Link>
                      <img
                        src={workout.picture_url}
                        alt={`${workout.name}`}
                        className="card-img-top workout-img"
                      />
                      <div className="d-flex align-items-center mt-2">
                        <input
                          type="text"
                          value={editableWorkout.name}
                          onChange={(event) => handleNameChange(event, workout)}
                          onBlur={handleInputBlur}
                          className="form-control"
                        />
                        <button
                          className="btn btn-primary ml-2"
                          onClick={handleSaveClick}
                        >
                          Save
                        </button>
                      </div>
                      <div className="mt-2">
                        <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteClick(workout)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            } else {
              return (
                <div className="col" key={workout.id}>
                  <div className="card workout-card">
                    <div className="card-body">
                      <Link
                        to={{
                          pathname: `/workouts/${workout.id}/exercises`,
                        }}
                        className="card-title"
                      >
                        <h1 style={{ color: "blue" }}>{workout.name}</h1>
                      </Link>
                      <img
                        src={workout.picture_url}
                        alt={`${workout.name}`}
                        className="card-img-top workout-img"
                      />
                      <div className="d-flex align-items-center mt-2">
                        <span className="card-text">{workout.name}</span>
                        <ion-icon
                          name="create-outline"
                          onClick={() => setEditableWorkout(workout)}
                          className="ml-2"
                          style={{ cursor: "pointer" }}
                        ></ion-icon>
                      </div>
                      <div className="mt-2">
                        <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteClick(workout)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          })}
        </div>
      </div>
    );
  }
}


export default WorkoutList;

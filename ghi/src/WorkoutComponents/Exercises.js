import { useParams } from "react-router-dom";
import { useDeleteExerciseMutation, useGetUserWorkoutExercisesQuery } from "../store/authApi";
import { Link } from "react-router-dom";
import { useState } from "react";
import "../App.css";
import toast from "react-hot-toast";


function ExerciseList() {
  const { workout_id } = useParams();
  const { data, isLoading, refetch } = useGetUserWorkoutExercisesQuery(workout_id);
  const [deleteExercise] = useDeleteExerciseMutation();
  const [exercises, setExercises] = useState(data?.exercises || []);


  const handleDeleteClick = async (exercise) => {
    if (
      window.confirm(
        `Are you sure you want to delete the exercise "${exercise.name}"?`
      )
    ) {
      await deleteExercise({ workout_id, id: exercise.id }).unwrap();
      toast.success('Deleted!');
      const updatedExercises = exercises.filter((e) => e.id !== exercise.id);
      setExercises(updatedExercises);
      await refetch();
    }
  };


  if (isLoading) {
    return (
      <progress className="progress is-primary" max="100">
        Loading
      </progress>
    );
  } else {
    return (
      <div className="container">
        <Link
          to={{
            pathname: `/workouts/${workout_id}/exercises/new`,
          }}
          className="btn btn-primary mb-3"
        >
          Add an Exercise
        </Link>
        <table className="table table-hover table-dark">
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Difficulty</th>
              <th>Instructions</th>
              <th>Muscle</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data?.exercises.map((exercise) => {
              return (
                <tr key={exercise.id}>
                  <td>{exercise.name}</td>
                  <td>{exercise.type}</td>
                  <td>{exercise.difficulty}</td>
                  <td className="scroll">{exercise.instructions}</td>
                  <td>{exercise.muscle}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDeleteClick(exercise)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}


export default ExerciseList;

import {
  useCreateExerciseMutation,
  useApiNinjasQuery,
  useGetUserWorkoutExercisesQuery,
} from "../store/authApi";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import "../App.css";
import toast  from "react-hot-toast";


function ExerciseForm() {
  const { workout_id } = useParams();
  const [createExercise, { isLoading }] = useCreateExerciseMutation();
  const [choice, setChoice] = useState({ muscle: "" });
  const { data: exercises, isFetching } = useApiNinjasQuery(choice.muscle, {
    skip: choice.muscle === "",
  });
  const { refetch } = useGetUserWorkoutExercisesQuery(workout_id);


  const handleChoice = (event) => {
    const value = event.target.value;
    setChoice({
      muscle: value,
    });
  };


  const handleAddClick = async (exercise) => {
    try {
      toast.success("Exercise Added Successfully!");
      await createExercise({
        workout_id: workout_id,
        exercise: exercise,
      }).unwrap();
      await refetch();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container">
      <div className="shadow p-4 mt-4">
        <h1>
          <Link
            to={{
              pathname: `/workouts/${workout_id}/exercises/`,
            }}
            className="btn btn-primary mb-3"
          >
            View Exercise List
          </Link>
        </h1>
        <h2 style={{ color: "white" }}>Exercises</h2>
        {isLoading && <p>Creating exercise...</p>}
        <div className="mb-3">
          <select
            onChange={handleChoice}
            value={choice.muscle}
            required
            name="muscle"
            id="muscle"
            className="form-select"
          >
            <option value="">Choose a Muscle</option>
            <option value="abdominals">abdominals</option>
            <option value="abductors">abductors</option>
            <option value="adductors">adductors</option>
            <option value="biceps">biceps</option>
            <option value="calves">calves</option>
            <option value="chest">chest</option>
            <option value="forearms">forearms</option>
            <option value="glutes">glutes</option>
            <option value="hamstrings">hamstrings</option>
            <option value="lats">lats</option>
            <option value="lower_back">lower_back</option>
            <option value="middle_back">middle_back</option>
            <option value="neck">neck</option>
            <option value="quadriceps">quadriceps</option>
            <option value="traps">traps</option>
            <option value="triceps">triceps</option>
          </select>
        </div>
        <div className="row row-cols-1 row-cols-md-3 g-4">
          {isFetching && <p>Loading...</p>}
          {!isFetching &&
            exercises?.map((exercise) => (
              <div className="col" key={exercise.name}>
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{exercise.name}</h5>
                    <p className="card-text">Type: {exercise.type}</p>
                    <p className="card-text">
                      Difficulty: {exercise.difficulty}
                    </p>
                    <p className="card-text1">
                      Instructions: {exercise.instructions}
                    </p>
                    <p className="card-text">Muscle: {exercise.muscle}</p>
                    <button
                      onClick={() => handleAddClick(exercise)}
                      className="btn btn-primary"
                    >
                      Add
                    </button>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}


export default ExerciseForm;

import { connect } from "react-redux";
import { useGetUserWorkoutsQuery } from "../store/authApi";
import { setWorkoutName, setWorkoutPictureUrl } from "../store/workoutSlice";
import { useCreateWorkoutMutation } from "../store/authApi";
import { useNavigate } from "react-router-dom";
import toast from "react-hot-toast";
import "../style/LoginSignUp.css"


function WorkoutForm({
  workoutName,
  workoutPictureUrl,
  setWorkoutName,
  setWorkoutPictureUrl,
  refetchWorkouts,
  tokenData,
}) {
  const [createWorkout] = useCreateWorkoutMutation();
  const navigate = useNavigate();


  const handleNameChange = (event) => {
    const value = event.target.value;
    setWorkoutName(value);
  };


  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setWorkoutPictureUrl(value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const data = { name: workoutName, picture_url: workoutPictureUrl };
      await createWorkout(data, {
        headers: { Authorization: `Bearer ${tokenData?.access_token}` },
      });
      setWorkoutName("");
      setWorkoutPictureUrl("");
      refetchWorkouts();
      toast.success('Workout Added!');
        setTimeout(() => {
        navigate("/workouts");
        }, 1000);

    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="login-box">
      <h2>Create New Workout</h2>
      <form onSubmit={handleSubmit} id="create-A-workout">
        <div className="user-box">
          <input
            onChange={handleNameChange}
            placeholder="Name"
            required
            type="text"
            name="name"
            id="style"
            className="form-control"
            value={workoutName}
          />
          <label htmlFor="name">Name *</label>
        </div>
        <div className="user-box">
          <input
            onChange={handlePictureUrlChange}
            placeholder="Picture URL"
            type="url"
            name="picture_url"
            id="style"
            className="form-control"
            value={workoutPictureUrl}
          />
          <label htmlFor="picture_url">Picture URL</label>
        </div>
        <>
          <button className="btn btn-outline-light" type="submit">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Create
          </button>
        </>
      </form>
    </div>
  );
}


const mapStateToProps = (state) => ({
  workoutName: state.workoutForm.workoutName,
  workoutPictureUrl: state.workoutForm.pictureUrl,
});


const mapDispatchToProps = (dispatch) => {
  return {
    setWorkoutName: (name) => dispatch(setWorkoutName(name)),
    setWorkoutPictureUrl: (picture_url) =>
      dispatch(setWorkoutPictureUrl(picture_url)),
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(function ConnectedWorkoutForm(props) {
  const refetchWorkouts = useGetUserWorkoutsQuery().refetch;

  return (
    <WorkoutForm
      {...props}
      refetchWorkouts={refetchWorkouts}
    />
  );
});

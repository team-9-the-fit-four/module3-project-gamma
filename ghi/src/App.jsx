import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SignUpForm from "./UserComponents/SignUp";
import LoginForm from "./UserComponents/Login";
import UserProfile from "./UserComponents/Profile";
import WorkoutList from "./WorkoutComponents/WorkoutList";
import ExerciseList from "./WorkoutComponents/Exercises";
import WorkoutForm from "./WorkoutComponents/WorkoutForm";
import ExerciseForm from "./WorkoutComponents/ExerciseForm";
import TrainerList from "./TrainerComponents/Trainer";



function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <div>
      <BrowserRouter basename={basename}>
        <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/signup" element={<SignUpForm />} />
          <Route path="/login" element={<LoginForm />} />
          <Route path="/profile" element={<UserProfile />} />
          <Route path="/workouts" element={<WorkoutList />} />
          <Route
            path="/workouts/:workout_id/exercises"
            element={<ExerciseList />}
          />
          <Route
            path="/workouts/:workout_id/exercises/new"
            element={<ExerciseForm />}
          />
          <Route path="/workouts/new" element={<WorkoutForm />} />
          <Route path="/workouts/:workout_id" element={<WorkoutForm />} />
          <Route path="/trainers" element={<TrainerList />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}


export default App;

import React, { useState, useEffect, useCallback } from "react";
import { useGetUserProfileQuery } from "../store/authApi";
import ProfileRow from "./ProfileRow";
import "../style/MainPage.css";
import "../style/Profile.css";


function UserProfile() {
  const { data } = useGetUserProfileQuery();
  const [user, setUser] = useState(null);

  const calculateBMI = useCallback(() => {
    if (user) return;
    const e = document.getElementsByName("h")[0].valueAsNumber;
    const t = document.getElementsByName("w")[0].valueAsNumber;
    const i = parseFloat((t / (e / 100) ** 2).toFixed(2));
    const h = [
      [0, 18.49],
      [18.5, 24.99],
      [25, 29.99],
      [30, 34.99],
      [35, 39.99],
      [40, 100],
    ].findIndex((e) => e[0] <= i && i < e[1]);
    let o = (0.393700787 * e).toFixed(0);
    document.getElementsByName("ho")[0].value = `${e} cm / ${Math.floor(
      o / 12
    )}' ${(o %= 12)}"`;
    document.getElementsByName("wo")[0].value = `${t} kg / ${(
      2.2046 * t
    ).toFixed(2)} lb`;
    if (h !== -1) {
      document.getElementsByName("g")[h].checked = true;
      document.getElementsByName("r")[0].value = i;
    }
  }, [user]);


  useEffect(() => {
  if (data) {
  setUser((prevUser) => {
    if (prevUser !== data.user) {
      return data.user;
    }
    return prevUser;
  });
  calculateBMI();
  }
  }, [data, calculateBMI]);


  const updateSliderRadioIcons = () => {
    const groups = document.querySelectorAll(".c-bmi__group-text");
    const radios = document.querySelectorAll('[name="g"]');
    groups.forEach((group, index) => {
      const radio = radios[index];
      if (radio.checked) {
        group.classList.add("active");
      } else {
        group.classList.remove("active");
      }
    });
  };

  return (
    <div className="home-page">
      <div className="card">
        {data?.users.map((user) => (
          <div className="card" key={user.id}>
            <ProfileRow
              user={user}
              setUser={setUser}
            />
            <form className="c-bmi">
              <label className="c-bmi__label">
                <strong>Height</strong>
                <input
                  className="c-bmi__range"
                  type="range"
                  name="h"
                  min="0"
                  max="305"
                  step="0.5"
                  defaultValue={user.height}
                  onChange={() => {
                    calculateBMI();
                    updateSliderRadioIcons();
                  }}
                />
                <output name="ho"></output>
              </label>
              <label className="c-bmi__label">
                <strong>Weight</strong>
                <input
                  className="c-bmi__range"
                  type="range"
                  name="w"
                  min="0"
                  max="453.6"
                  step="0.1"
                  defaultValue={user.weight}
                  onChange={() => {
                    calculateBMI();
                    updateSliderRadioIcons();
                  }}
                />
                <output name="wo"></output>
              </label>
              <div className="c-bmi__result">
                Your BMI Is: <output name="r"></output>
              </div>
              <div className="c-bmi__groups" readOnly>
                <input type="radio" id="bmi-g0" name="g" />
                <label htmlFor="bmi-g0">Underweight</label>
                <div className="c-bmi__group-text">
                  The WHO regards a BMI of less than 18.5 as underweight and may
                  indicate malnutrition, an eating disorder, or other health
                  problems.
                </div>
                <input type="radio" id="bmi-g1" name="g" defaultChecked />
                <label htmlFor="bmi-g1">Normal</label>
                <div className="c-bmi__group-text">
                  A BMI between 18.5 and 25 is considered normal and healthy.{" "}
                </div>
                <input type="radio" id="bmi-g2" name="g" />
                <label htmlFor="bmi-g2">Pre-obesity</label>
                <div className="c-bmi__group-text">
                  People who fall into this category may be at risk of
                  developing obesity.
                  <br />
                  This was earlier classified as "overweight".
                </div>
                <input type="radio" id="bmi-g3" name="g" />
                <label htmlFor="bmi-g3">Obese I</label>
                <div className="c-bmi__group-text">
                  People who have BMI equal or over 30 may have obesity, which
                  is defined as an abnormal or excessive accumulation of fat
                  that may harm health.
                </div>
                <input type="radio" id="bmi-g4" name="g" />
                <label htmlFor="bmi-g4">Obese II</label>
                <div className="c-bmi__group-text">
                  People who have BMI equal or over 30 may have obesity, which
                  is defined as an abnormal or excessive accumulation of fat
                  that may harm health.
                </div>
                <input type="radio" id="bmi-g5" name="g" />
                <label htmlFor="bmi-g5">Obese III</label>
                <div className="c-bmi__group-text">
                  People who have BMI equal or over 30 may have obesity, which
                  is defined as an abnormal or excessive accumulation of fat
                  that may harm health.
                </div>
              </div>
            </form>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserProfile;

import { useEffect, useCallback } from "react";
import { useNavigate, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useLogInMutation } from "../store/authApi";
import { updateField } from "../store/userSlice";
import { Alert } from "react-bootstrap";
import toast from "react-hot-toast";
import "../style/LoginSignUp.css";


function LoginForm() {
  const dispatch = useDispatch();
  const { username, password } = useSelector((state) => state.user);
  const [
    logIn,
    { error, isLoading: logInLoading, isSuccess: loginSuccessful },
  ] = useLogInMutation();


  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );


  const navigate = useNavigate();


const handleLogin = useCallback(async () => {
  if (loginSuccessful) {
    toast.success("Login Successful");
    setTimeout(() => {
      navigate("/");
    }, 800);
  }
}, [loginSuccessful, navigate]);

  useEffect(() => {
    handleLogin();
  }, [handleLogin]);

  return (
    <div className="login-box">
      <h2>Log In</h2>
      {error ? <Alert variant="danger">{error.data.detail}</Alert> : null}
      <form
        method="POST"
        onSubmit={async (e) => {
          e.preventDefault();
          await logIn(e.target);
          handleLogin();
        }}
      >
        <div className="user-box">
          <input
            required
            onChange={field}
            value={username}
            name="username"
            className="form-control"
            type="email"
            placeholder="email"
          />
          <label htmlFor="email">Email</label>
        </div>
        <div className="user-box">
          <input
            required
            onChange={field}
            value={password}
            name="password"
            className="form-control"
            type="password"
            placeholder="password"
          />
          <label className="label">Password</label>
        </div>
        <>
          <button
            disabled={logInLoading}
            className="btn btn-outline-light"
            onClick={handleLogin}
            type="submit"
          >
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Log In
          </button>
        </>
      </form>
      <div className="vertical-spacer"></div>
      <p>
        Don't Have an Account?{" "}
        <Link className="link-info" to="/signup">
          Create Account
        </Link>
      </p>
    </div>
  );
}


export default LoginForm;

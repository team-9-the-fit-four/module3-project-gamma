import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSignUpMutation } from "../store/authApi";
import { clearForm, updateField } from "../store/userSlice";
import { Alert } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import toast from "react-hot-toast";
import "../style/LoginSignUp.css"


function SignUpForm() {
  const dispatch = useDispatch();
  const { username, password, first, last, age, weight, height, photo } = useSelector((state) => state.user);
  const navigate = useNavigate();

  const [
    signUp,
    { error, isSignedUp },
  ] = useSignUpMutation();

  const handleFormChange = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  const handleSignUp = useCallback(
    async (event) => {
      event.preventDefault();

      const data = {
        email: username,
        password: password,
        first: first,
        last: last,
        age: age,
        weight: weight,
        height: height,
        photo: photo,
      };
      await signUp(data);

      if (!isSignedUp) {
        toast.success("Sign Up Successful!")
        dispatch(clearForm());
        setTimeout(() => {
          navigate("/login");
        }, 800);
      }
    },
    [username, password, first, last, age, weight, height,
      photo, signUp, isSignedUp, dispatch, navigate]
  );

  return (
    <div className="login-box">
      <h2>Sign Up</h2>
      {error ? <Alert variant="danger">{error.data.detail}</Alert> : null}
      <form onSubmit={handleSignUp} id="Sign Up">
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={username}
            placeholder="email"
            required
            type="email"
            name="username"
            id="username"
            className="form-control"
          />
          <label htmlFor="email">Email *</label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={password}
            placeholder="password"
            required
            type="password"
            name="password"
            id="password"
            className="form-control"
          />
          <label htmlFor="password">Password *</label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={first}
            placeholder="first"
            required
            type="text"
            name="first"
            id="first"
            className="form-control"
          />
          <label htmlFor="first">First Name *</label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={last}
            placeholder="last"
            type="text"
            name="last"
            id="last"
            className="form-control"
          />
          <label htmlFor="last">Last Name</label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={age}
            placeholder="age"
            type="number"
            name="age"
            id="age"
            className="form-control"
          />
          <label htmlFor="age">Age (years) </label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={height}
            placeholder="height"
            type="number"
            name="height"
            id="height"
            className="form-control"
          />
          <label htmlFor="height">Height (cm) </label>
        </div>
        <div className="user-box">
          <input
            onChange={handleFormChange}
            value={weight}
            placeholder="weight"
            type="number"
            name="weight"
            id="weight"
            className="form-control"
          />
          <label htmlFor="weight">Weight (kg) </label>
        </div>
        <>
          <button
            className="btn btn-primarybtn btn-outline-light"
            type="submit"
          >
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Sign Up
          </button>
        </>
      </form>
    </div>
  );
}

export default SignUpForm;

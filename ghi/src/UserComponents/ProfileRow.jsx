import React, { useState } from "react";
import {
  useUpdateUserMutation,
  useGetUserProfileQuery,
} from "../store/authApi";
import toast from "react-hot-toast";
import "../style/RocketButton.css";


const useSaveUserData = () => {
  const [updateUser] = useUpdateUserMutation();
  const { refetch } = useGetUserProfileQuery();

  const saveUserData = async (userId, formData) => {
    const result = await updateUser({ user_id: userId, data: formData });
    if (result.data) {
      await refetch();
      return result.data.user;
    }
    throw new Error("Failed to update user");
  };

  return saveUserData;
};

const ProfileRow = ({ user, setUser }) => {
  const [showEdit, setShowEdit] = useState(false);
  const [formData, setFormData] = useState({
    first: user.first,
    last: user.last,
    age: user.age,
    weight: user.weight,
    height: user.height,
  });

  const saveUserData = useSaveUserData();

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSave = async () => {
    toast.success("Changes Saved!");
    const updatedUser = await saveUserData(user.id, formData);
    setUser(updatedUser);
    setShowEdit(false);
  };

  return (
    <div className="profile-row">
      <div>
        <strong>First Name: </strong>
        {!showEdit ? (
          <span>{user.first}</span>
        ) : (
          <input
            style={{ width: "500px" }}
            type="text"
            value={formData.first}
            name="first"
            onChange={handleChange}
          />
        )}
      </div>
      <div>
        <strong>Last Name: </strong>
        {!showEdit ? (
          <span>{user.last}</span>
        ) : (
          <input
            style={{ width: "500px" }}
            type="text"
            value={formData.last}
            name="last"
            onChange={handleChange}
          />
        )}
      </div>
      <div>
        <strong>Age (years): </strong>
        {!showEdit ? (
          <span>{user.age}</span>
        ) : (
          <input
            style={{ width: "500px" }}
            type="number"
            value={formData.age}
            name="age"
            onChange={handleChange}
          />
        )}
      </div>
      <div>
        <strong>Height (cm): </strong>
        {!showEdit ? (
          <span>{user.height}</span>
        ) : (
          <input
            style={{ width: "500px" }}
            type="number"
            value={formData.height}
            name="height"
            onChange={handleChange}
          />
        )}
      </div>
      <div>
        <strong>Weight (kg): </strong>
        {!showEdit ? (
          <span>{user.weight}</span>
        ) : (
          <input
            style={{ width: "500px" }}
            type="number"
            value={formData.weight}
            name="weight"
            onChange={handleChange}
          />
        )}
      </div>
      {!showEdit && (
        <div id="pulsa" className="pulsa">
          <div className="vertical-spacer-rocket"></div>
          <button className="cta" id="cta" onClick={() => setShowEdit(true)}>
            EDIT
            <div className="star a"></div>
            <div className="star b"></div>
            <div className="star c"></div>
            <div className="star d"></div>
            <div className="star e"></div>
            <div className="star f"></div>
            <div className="star g"></div>
            <svg
              version="1.1"
              viewBox="0 0 1024 1024"
              width="24px"
              className="rocket"
            >
              <path
                fill="#3A7EB9"
                d="M662.72 462.784l136.448 169.408v173.248l-136.448-48.32zM342.72 457.344L206.272 626.816v173.248l136.448-48.384z"
              ></path>
              <path
                fill="#D48171"
                d="M570.688 418.688l-142.848 1.152a266.496 266.496 0 0 1-20.288-0.576l3.712 448.64c0.256 28.928 94.272 130.048 94.272 130.048s93.888-102.656 93.632-131.584l-3.712-448.96a344.64 344.64 0 0 1-24.768 1.28z"
              ></path>
              <path
                fill="#E9DF92"
                d="M531.456 599.296l-63.04 0.576c-3.008 0-5.952-0.064-8.96-0.384l2.176 257.792c0.128 16.64 41.728 74.816 41.728 74.816s41.344-58.944 41.28-75.52l-2.176-257.92c-3.648 0.384-7.296 0.64-11.008 0.64z"
              ></path>
              <path
                fill="#B5D5EB"
                d="M554.304 93.568a324.352 324.352 0 0 0-110.592 1.728L342.72 240.768v584.512c13.824-0.96 27.968-1.536 42.368-1.536h245.248c11.84 0 23.36 0.384 34.816 1.024V253.312L554.304 93.568z"
              ></path>
              <path
                fill="#3A7EB9"
                d="M541.44 94.144L500.416 35.008l-45.696 59.136v29.504h89.024v-29.504zM459.456 288.64h88.96v88.896h-88.96zM459.456 467.456h88.96v88.96h-88.96zM459.456 634.176h88.96v88.896h-88.96zM364.928 788.736h277.76v44.352h-277.76z"
              ></path>
            </svg>
          </button>
        </div>
      )}
      {showEdit && (
        <div id="pulsa" className="pulsa">
          <div className="vertical-spacer-rocket"></div>
          <button className="cta" id="cta" onClick={handleSave}>
            SAVE
            <div className="star a"></div>
            <div className="star b"></div>
            <div className="star c"></div>
            <div className="star d"></div>
            <div className="star e"></div>
            <div className="star f"></div>
            <div className="star g"></div>
            <svg
              version="1.1"
              viewBox="0 0 1024 1024"
              width="24px"
              className="rocket"
            >
              <path
                fill="#3A7EB9"
                d="M662.72 462.784l136.448 169.408v173.248l-136.448-48.32zM342.72 457.344L206.272 626.816v173.248l136.448-48.384z"
              ></path>
              <path
                fill="#D48171"
                d="M570.688 418.688l-142.848 1.152a266.496 266.496 0 0 1-20.288-0.576l3.712 448.64c0.256 28.928 94.272 130.048 94.272 130.048s93.888-102.656 93.632-131.584l-3.712-448.96a344.64 344.64 0 0 1-24.768 1.28z"
              ></path>
              <path
                fill="#E9DF92"
                d="M531.456 599.296l-63.04 0.576c-3.008 0-5.952-0.064-8.96-0.384l2.176 257.792c0.128 16.64 41.728 74.816 41.728 74.816s41.344-58.944 41.28-75.52l-2.176-257.92c-3.648 0.384-7.296 0.64-11.008 0.64z"
              ></path>
              <path
                fill="#B5D5EB"
                d="M554.304 93.568a324.352 324.352 0 0 0-110.592 1.728L342.72 240.768v584.512c13.824-0.96 27.968-1.536 42.368-1.536h245.248c11.84 0 23.36 0.384 34.816 1.024V253.312L554.304 93.568z"
              ></path>
              <path
                fill="#3A7EB9"
                d="M541.44 94.144L500.416 35.008l-45.696 59.136v29.504h89.024v-29.504zM459.456 288.64h88.96v88.896h-88.96zM459.456 467.456h88.96v88.96h-88.96zM459.456 634.176h88.96v88.896h-88.96zM364.928 788.736h277.76v44.352h-277.76z"
              ></path>
            </svg>
          </button>
        </div>
      )}
      <div className="vertical-spacer"></div>
    </div>
  );
};

export default ProfileRow;
